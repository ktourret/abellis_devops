FROM nginx:1.18

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

COPY ./dist/abellisdevops /usr/share/nginx/html
