import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pokedex-third',
  templateUrl: './pokedex-third.component.html',
  styleUrls: ['./pokedex-third.component.scss']
})
export class PokedexThirdComponent implements OnInit {

  baseurl: String = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/";
  suffix: String = ".png";
  pokeindex: number[] =[];

  constructor() { 
  
  }

  ngOnInit(): void {
      for(let i=252;i<=386;i++)
      {
         this.pokeindex.push(i);
      }
  }

}
