import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokedexThirdComponent } from './pokedex-third.component';

describe('PokedexThirdComponent', () => {
  let component: PokedexThirdComponent;
  let fixture: ComponentFixture<PokedexThirdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokedexThirdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PokedexThirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
