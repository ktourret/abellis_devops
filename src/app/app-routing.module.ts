import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MypokeComponent } from './mypoke/mypoke.component';
import {JohtoComponent} from './johto/johto.component';
import {PokedexThirdComponent} from './pokedex-third/pokedex-third.component';
import {Pokedex4Component} from './pokedex4/pokedex4.component';

const routes: Routes = [
  { path: 'gen-4', component: Pokedex4Component },
  { path: 'mypoke', component: MypokeComponent },
  { path: 'johto', component: JohtoComponent },
  { path: 'hoenn', component: PokedexThirdComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
