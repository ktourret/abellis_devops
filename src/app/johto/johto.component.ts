import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'johto',
  templateUrl: './johto.component.html',
  styleUrls: ['./johto.component.scss']
})
export class JohtoComponent implements OnInit{
  ids: number[] = []
  base_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/"
  start_id = 152
  end_id = 251

  ngOnInit(): void {
    for (let i = this.start_id; i <= this.end_id; i++) {
      this.ids.push(i)
    }
  }
}
