import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Pokedex4Component } from './pokedex4/pokedex4.component';

import { PokedexThirdComponent } from './pokedex-third/pokedex-third.component';
import { JohtoComponent } from './johto/johto.component';
import { MypokeComponent } from './mypoke/mypoke.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    Pokedex4Component,
    PokedexThirdComponent,
    MypokeComponent,
    JohtoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
