import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pokedex4',
  templateUrl: './pokedex4.component.html',
  styleUrls: ['./pokedex4.component.scss']
})
export class Pokedex4Component implements OnInit {

  index: number[] = [];

  constructor() { }

  ngOnInit(): void {
    for (let i = 387; i <= 493; i++) {
      this.index.push(i);
    }
  }

}
