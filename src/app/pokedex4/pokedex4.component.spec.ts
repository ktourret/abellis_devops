import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pokedex4Component } from './pokedex4.component';

describe('Pokedex4Component', () => {
  let component: Pokedex4Component;
  let fixture: ComponentFixture<Pokedex4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pokedex4Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Pokedex4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
